\contentsline {chapter}{\numberline {1}REQUISITI}{3}% 
\contentsline {chapter}{\numberline {2}PROGETTAZIONE ARDUINO}{6}% 
\contentsline {section}{\numberline {2.1}GreenHouseController}{6}% 
\contentsline {subsection}{\numberline {2.1.1}Task}{6}% 
\contentsline {subsubsection}{2.1.1.1 Led}{6}% 
\contentsline {subsubsection}{2.1.1.2 Servo Motore}{7}% 
\contentsline {subsubsection}{2.1.1.3 Sonar}{8}% 
\contentsline {subsubsection}{2.1.1.4 BTMessage}{9}% 
\contentsline {subsubsection}{2.1.1.5 GreenController}{10}% 
\contentsline {chapter}{\numberline {3}PRO E CONTRO}{12}% 
\contentsline {section}{\numberline {3.1}PRO:}{12}% 
\contentsline {section}{\numberline {3.2}CONTRO:}{12}% 
\contentsline {chapter}{\numberline {4}PERIODO DEI VARI TASK}{13}% 
\contentsline {chapter}{\numberline {5}PROGETTAZIONE ESP}{14}% 
\contentsline {section}{\numberline {5.1}GreenHouseEdge}{14}% 
\contentsline {chapter}{\numberline {6}PROGETTAZIONE SERVER}{15}% 
\contentsline {section}{\numberline {6.1}GreenHouseServer}{15}% 
\contentsline {subsection}{\numberline {6.1.1}GreenHouseConsole.java}{15}% 
\contentsline {subsection}{\numberline {6.1.2}Controller.java}{15}% 
\contentsline {subsection}{\numberline {6.1.3}MonitoringMessage.java}{16}% 
\contentsline {subsection}{\numberline {6.1.4}Serverconnectjdbc.java}{16}% 
\contentsline {subsection}{\numberline {6.1.5}DataService.java}{17}% 
\contentsline {chapter}{\numberline {7}PROGETTAZIONE DATABASE}{18}% 
\contentsline {section}{\numberline {7.1}greenhouseserver DATABASE}{18}% 
\contentsline {chapter}{\numberline {8}PROGETTAZIONE ANDROID}{20}% 
\contentsline {section}{\numberline {8.1}GreenHouseMobileApp}{20}% 
\contentsline {subsection}{\numberline {8.1.1}Classi riutilizzate}{20}% 
\contentsline {subsection}{\numberline {8.1.2}MainActivity}{21}% 
\contentsline {chapter}{\numberline {9}PROGETTAZIONE FRONT-END}{23}% 
\contentsline {section}{\numberline {9.1}GreenHouseFrontEnd}{23}% 
\contentsline {section}{\numberline {9.2}RIEPILOGO}{24}% 
\contentsline {chapter}{\numberline {10}SCHEMA FRITZING DEL SISTEMA }{26}% 
