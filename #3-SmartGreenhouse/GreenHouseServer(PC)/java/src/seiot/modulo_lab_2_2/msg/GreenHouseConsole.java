package seiot.modulo_lab_2_2.msg;
import seiot.modulo_lab_2_2.msg.Serverconnectjdbc;
import java.sql.SQLException;
import seiot.modulo_lab_4_1.DataService;

class GreenHouseConsole   {

	static Serverconnectjdbc databaseConnector = null;
	static DataService service;
	


	public static void main(String[] args) throws Exception {
		String portName = "COM3";
		try {
			databaseConnector = new Serverconnectjdbc();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
		Controller contr = new Controller(portName, databaseConnector);
		databaseConnector.registerController(contr);		
	}
}
