package seiot.modulo_lab_2_2.msg;
import seiot.modulo_lab_2_2.msg.jssc.*;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;
import java.sql.*;

public class MonitoringMessage extends Thread {

	SerialCommChannel channel;
	Serverconnectjdbc db;
	
	Calendar c;
	Timestamp dataInizio;
	Timestamp dataFine;
	java.sql.Date data;
	
	Date durata;
	int value=0;
	String attention;
	float umdi;
	float umdf;
	float umidity;
	String state;
	
	String secondi;
	String minuti;
	String ora;
	Timestamp dateToApp;
	Calendar oggi;

	static final String MSG_IRRIGATION		= "irr";
	static final String MSG_ATTENTION 		= "att";
	static final String MSG_UMIDITA_INIZIALE 		= "umI";
	static final String MSG_UMIDITA_FINALE 		= "umf";
	static final String MSG_STATE 		= "s";

	public MonitoringMessage(SerialCommChannel channel, Serverconnectjdbc databaseConnector) throws Exception {
		this.channel = channel;
		this.db = databaseConnector;
		this.dataFine=null;
		this.umdf=0;
		this.umdi=0;
	}

	public void run(){
		while (true){
			try {
				String msg = channel.receiveMsg();
				if(msg.length()>0) {
					System.out.println("[FROM ARDUINO]" + msg);
				}
				if (msg.startsWith(MSG_IRRIGATION)){
					this.c=Calendar.getInstance();
					this.data=new java.sql.Date(this.c.getTime().getTime());
					this.dataInizio=new Timestamp(this.data.getTime());
					value++;
					}else{
							if (msg.startsWith(MSG_ATTENTION )){
						 		this.c=Calendar.getInstance();
								this.data=new java.sql.Date(this.c.getTime().getTime());
								this.dataFine=new Timestamp(this.data.getTime());
								attention=msg;
								value++;
							}else{
								 		if (msg.startsWith(MSG_UMIDITA_INIZIALE )){
											umdi=this.umidity;
											value++;
										}else{
										 		if (msg.startsWith(MSG_UMIDITA_FINALE)){
													umdf=this.umidity;
													value++;
												}else {
													if (msg.startsWith(MSG_STATE)){
														this.state=msg;
														this.c=Calendar.getInstance();
														this.data=new java.sql.Date(this.c.getTime().getTime());
														this.dateToApp=new Timestamp(this.data.getTime());
														try {
															db.insertApp(this.state, String.valueOf(this.umidity), this.dateToApp);
														} catch (SQLException e) {
															e.printStackTrace();
														}
														this.state="";
														this.dateToApp=null;
													}
												}
											}
										}
									}
						if(value==4){
							db.insert(dataInizio, attention, String.valueOf(umdi), String.valueOf(umdf), dataFine);
							value=0;
							dataFine=null;
						}
			} catch (Exception ex){
				ex.printStackTrace();
			}
		}
	}

	public void sendHumidity(float humidity) {
		channel.sendMsg(String.valueOf(humidity));
		System.out.println("[TO ARDUINO]" + humidity);
		this.umidity=humidity;
	}

}
