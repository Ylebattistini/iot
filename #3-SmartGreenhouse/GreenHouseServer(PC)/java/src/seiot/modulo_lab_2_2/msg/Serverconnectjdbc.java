package seiot.modulo_lab_2_2.msg;
import java.sql.*;

class Serverconnectjdbc {
	private Connection conn=null;
	private String driver;
	private String url;
	private String utente;
	private String psw;
	private Statement stmt;
  public Serverconnectjdbc() throws SQLException {
   
     this.driver="com.mysql.cj.jdbc.Driver";
     this.url="jdbc:mysql://localhost:3306/greenhouseserver?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=Europe/Rome";
     this.utente="root";
     this.psw="";
     try {
    	 System.out.println("DataSource.getConnection() driver = " + this.driver);
    	 Class.forName(driver);
    	 System.out.println("DataSource.getConnection() dbUri = " + this.url);
		 this.conn = DriverManager.getConnection(this.url, this.utente, this.psw);
			System.out.println("Connessione stabilita");

		} catch (ClassNotFoundException e) {
			new Exception(e.getMessage());
			System.out.println("Errore" + e.getMessage());
		}         

          this.stmt = conn.createStatement();
          //this.stmt.executeUpdate("Create table IF NOT EXISTS green(id INT PRIMARY KEY AUTO_INCREMENT ,dataInizio DATETIME , descrizione VARCHAR(100), umidita_iniziale VARCHAR(20), umidita_finale VARCHAR(20), dataFineDATETIME)");
          //this.stmt.executeUpdate("Create table IF NOT EXISTS infoapp(idApp INT PRIMARY KEY AUTO_INCREMENT ,state VARCHAR(50) , umidity VARCHAR(20),  ora DATETIME)");
   }

  public void registerController(Controller contr){

  }
  

  public void insert(Timestamp dataInizioi, String descrizionei, String umiditainizialei, String umiditafinalei, Timestamp dataFinei) throws SQLException {
    // inserisco i record
	  
	  String sSQL="INSERT INTO green(dataInizio, descrizione, umidita_iniziale, umidita_finale, dataFine) values ( ?, ?, ?, ?, ?)";
		try {
			// create the mysql insert preparedstatement
			PreparedStatement preparedStmt = conn.prepareStatement(sSQL);
			preparedStmt.setTimestamp(1, dataInizioi);
			preparedStmt.setString(2, descrizionei);
			preparedStmt.setString(3, umiditainizialei);
			preparedStmt.setString(4, umiditafinalei);
			preparedStmt.setTimestamp(5, dataFinei);

			// execute the preparedstatement
			preparedStmt.execute();

		} catch (Exception e) {
			System.out.println(e);
		}
  }

	public void insertApp(String state, String umidity, Timestamp dateToApp) throws SQLException {
		String sSQL1="INSERT INTO infoapp ( state, umidity, ora) values (?, ?, ?)";
		try {
			// create the mysql insert preparedstatement
			PreparedStatement preparedStmt = conn.prepareStatement(sSQL1);
			preparedStmt.setString(1, state);
			preparedStmt.setString(2, umidity);
			preparedStmt.setTimestamp(3, dateToApp);

			// execute the preparedstatement
			preparedStmt.execute();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

}

