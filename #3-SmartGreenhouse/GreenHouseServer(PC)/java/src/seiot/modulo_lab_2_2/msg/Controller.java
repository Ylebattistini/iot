package seiot.modulo_lab_2_2.msg;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import io.vertx.core.Vertx;
import seiot.modulo_lab_2_2.msg.jssc.*;
import seiot.modulo_lab_4_1.DataService;

public class Controller implements ActionListener {

	SerialCommChannel channel;
	DataService service;
	MonitoringMessage monitoring;
	static Vertx vertx;

	public Controller(String port,Serverconnectjdbc databaseConnector) throws Exception {
		channel = new SerialCommChannel(port,9600);
		monitoring=new MonitoringMessage(channel, databaseConnector);
		service = new DataService(8080, this);
		service.setmain(service);
		monitoring.run();		

		/* attesa necessaria per fare in modo che Arduino completi il reboot */

		System.out.println("Waiting Arduino for rebooting...");
		Thread.sleep(4000);
		System.out.println("Ready.");
	}
	
	public void actionPerformed(ActionEvent ev){
		 
	 }

	public void sendHumidity(float humidity) {
		this.monitoring.sendHumidity(humidity);
		System.out.print("[SONO IL CONTROLLER] umidita  " + humidity);
	}

}
