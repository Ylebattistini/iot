#ifndef __HUMIDITY__
#define __HUMIDITY__

class Humidity {
 
public: 
  virtual float getValue() = 0;
};

#endif
