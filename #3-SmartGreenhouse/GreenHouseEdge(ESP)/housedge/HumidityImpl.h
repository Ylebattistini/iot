#ifndef __HUMIDITYIMPL__
#define __HUMIDITYIMPL__

#include "Humidity.h"
#include "DHT.h"

class HumidityImpl : public Humidity{
 
public: 
  HumidityImpl(int hum_pin);
  float getValue();

private:
  int humidity_Pin;
};

#endif
