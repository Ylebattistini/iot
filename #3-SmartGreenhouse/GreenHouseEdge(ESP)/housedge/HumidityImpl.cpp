#include "HumidityImpl.h"
#include "Arduino.h"
#include "DHT.h"

#define DHTTYPE DHT11


HumidityImpl::HumidityImpl(int hum_pin)
{
    this->humidity_Pin = hum_pin;
}

float HumidityImpl::getValue()
{
    DHT dht(humidity_Pin, DHTTYPE);
    return dht.readHumidity();
}
