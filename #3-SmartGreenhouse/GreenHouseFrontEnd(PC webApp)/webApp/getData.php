<?php
require("config.php");
require("DB_PDO/PDO.php");
use DB\DBconnection;

$connDB = DBconnection::getConn(DSN, USER);

//storico irrigazioni
$storico = $connDB->prepare('SELECT id, dataInizio, descrizione, umidita_iniziale, umidita_finale, DATE_FORMAT((dataFine - dataInizio), "%h:%i:%s") as durata FROM green ORDER BY id DESC');
$storico->execute();
$storico = $storico->fetchAll();

if(count($storico) > 10){
    $connDB->exec("DELETE FROM green WHERE id <=" . ($storico[0]['id'] - 10));
}

$irrigazioni = "";

foreach($storico as $row){ 
    $irrigazioni = $irrigazioni . "<tr>
                                    <td>" . $row['dataInizio'] . "</td>
                                    <td>" . $row['durata'] . "</td>
                                    <td>" . $row['umidita_iniziale'] . "</td>
                                    <td>" . $row['umidita_finale'] . "</td>
                                    <td>" . $row['descrizione'] . "</td>
                                    </tr>";
}

$connDB = null;

echo $irrigazioni;
?>