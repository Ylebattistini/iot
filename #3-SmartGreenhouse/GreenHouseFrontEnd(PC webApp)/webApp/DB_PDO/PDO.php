<?php

/* 
    Umberto Baldini
    classe per l'utilizzo generico di connessioni a database tramite php PDO
    class for generic use of DB connection using php PDO
*/

namespace DB {

    class DBconnection {

        private $conn;
        private static $instance = null;

        //singleton
        private function __construct($dsn, $user) {
            try {
                $this->conn = new \PDO($dsn, $user, "", array());
            } catch (PDOException $e) {
                echo 'Connection failed: ' . $e->getMessage();
            }
        }

        //to obtain the instance
        public static function getConn($dsn, $user){
            if (self::$instance === null){
                self::$instance = new DBconnection($dsn, $user);
            }
            return self::$instance; 
        }

        //to prepare a query
        public function prepare($query){
            try{
                return $this->conn->prepare($query);
            } catch (PDOException $e) {
                echo 'prepared statement failed: ' . $e->getMessage();
            }
        }
        
        //to retrieve the last inserted id
        public function lastInsertId(){
            return $this->conn->lastInsertId();
        }

        //to execute a query
        public function exec($query){
            try{
                return $this->conn->query($query);
            } catch (PDOException $e) {
                echo 'query execution failed: ' . $e->getMessage();
            }
        }


        
        //to prevent the singleton clone
        private function __clone()
        {
        }

        //to prevent unserialize of singleton instance
        private function __wakeup()
        {
        }

    }
}