//stampa il grafico dell'umidità
/*function createChart(){   
    var time = [];
    var umidita = [];

    var ctx = document.getElementById('myChart').getContext('2d');
    ctx.canvas.width = 400;
    ctx.canvas.height = 400;
   var myChart = new Chart(ctx, {
        type: 'line',
        data: { 
            labels: time,
            datasets: [{
                label: "orario di lettura del dato",
                data: umidita,
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                pointRadius: 3,
                fill: false,
                lineTension: 0,
                borderWidth: 2
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    scaleLabel: {
                        labelString: 'umdidità in %'
                    },
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

}*/



function refresh() {

    getDati();

    getStato();

    //ciclo di aggiornamento tabelle
    // chart.update();
    setTimeout(refresh, 5000);
}

function getDati(){
        // make Ajax call here, inside the callback call:
        var dati = $.ajax({
            type: "POST",
            url: "./getData.php",
            dataType: "html",
            error: function(){
                alert("impossibile richiedere i dati");
            }
        });
    
        dati.done(function(result){
            $("#tabStorico").html('<thead class="thead-dark"><tr><th scope="col">DATA E ORA </th><th scope="col">DURATA </th><th scope="col">UMIDITA\' INIZIALE </th><th scope="col">UMIDITA\' FINALE </th><th scope="col">SEGNALAZIONI </th></tr></thead>' + result);
        }).fail(function(richiesta, status, error_verbose){
            alert(status + " " + error_verbose);
        });
}

function getStato(){
    var stato = $.ajax({
        type: "POST",
        url: "./getStato.php",
        dataType: "html",
        error: function(){
            alert("impossibile richiedere i dati");
        }
    });

    stato.done(function (result){
        $("#stato").html('<h4 class="text-left">Stato: <span id="stato">' + result + '</span></h4>');
    }).fail(function(richiesta, status, error_verbose){
        alert(status + " " + error_verbose);
    });
}

$(document).ready(function(){
    //funzione per il polling dei dati
    refresh();
});