#include"ServoMotore.h"
#include"Arduino.h"
#include"config.h"
#include "MsgService.h"

extern bool pompaopen;
extern bool apri;
extern bool chiudi;
extern int portata;
extern bool automatic;
extern float umidita;
extern bool irrigazione;
extern bool manual;
Msg*data;
String messaggio;

ServoMotore::ServoMotore(int pin){
  this->servoPin=pin;
  this->U=1000;
  this->pos=0;
  this->delta=1;
  this->i=0;
  this->timer=0;
}

void ServoMotore::init(int period){
    Task::init(period);
}

void ServoMotore::on(){
   motor.attach(this->servoPin);    
}

void ServoMotore::setPosition(int angle){
  // 750 -> 0, 2250 -> 180 
  // 750 + angle*(2250-750)/180
  float coeff = (2250.0-750.0)/180;
  motor.write(750 + angle*coeff);              
}

void ServoMotore::off(){
  motor.detach();    
}

void ServoMotore::irrigation(){
  data=(MsgService.receiveMsg());
  messaggio=data->getContent();
  if(automatic==true){
     MsgService.sendMsg("s:automatico\n");
  }else{
    if(manual==true){
       MsgService.sendMsg("s:manuale\n");
    }
  }
  if(messaggio.toFloat()>0.0){
      this->U=messaggio.toFloat();
      umidita=this->U;
  }
  MsgService.sendMsg("[SONO ARDUINO]"+messaggio);
  MsgService.sendMsg("[SONO ARDUINO TO FLOAT]"+String(this->U));
  if((this->U < Umin && automatic==true) || apri==true){
      pompaopen=true; 
      irrigazione=true;
      
      if(this->i==0){
        MsgService.sendMsg("irr:Inizio irrigazione!\n");
        MsgService.sendMsg("umI:" + String(this->U));
        this->i++;
      }
      
      if((this->U>20.0 && this->U<=30.0) || portata==Pmin){//Pmin
        portata=Pmin;
        this->pos=0;
        setPosition(portata);
        delay(100);
        on();
           for (int i = 0; i < 180; i++) {
              setPosition(this->pos);         
              delay(5);            
              this->pos += this->delta;
            }

      }else{
            if((this->U>10.0 && this->U<=20.0)|| portata==Pmed){
              portata=Pmed;
              this->pos=0;
              setPosition(portata);
              on();
                for (int i = 0; i < 180; i++) {
                    setPosition(this->pos);         
                    delay(5);            
                    this->pos += this->delta;
                }
             
            }else{
                  if((this->U<=10)|| portata==Pmax){
                    portata=Pmax;
                    this->pos=0;
                    setPosition(portata);
                    on();
                      for (int i = 0; i < 180; i++) {
                          setPosition(this->pos);         
                          delay(5);            
                          this->pos += this->delta;
                      }
                   
                  }else{
                    off();
                  }
            }
      }
      
      if((U<Umin+DeltaU && irrigazione==true)|| (apri==true && manual==true)){
         timer++;
      }

      if((U>Umin+DeltaU && pompaopen==true) || (chiudi==true && pompaopen==true)){
        off();
        irrigazione=false;
        this->pos=0;
        this->i=0;
        if(timer<Tmax ){
            MsgService.sendMsg("att:Nessun problema, l'irrigazione è avvenuta correttamente!!\n");
        }
        this->timer=0;
        pompaopen=false;
        apri=false;
        chiudi=false;
        MsgService.sendMsg("umf:" + String(this->U));
      }
  }

  if(timer >=Tmax && irrigazione==true){  
    MsgService.sendMsg("att:Attenzione il tempo dell'irrigazione ha superato il tempo massimo!\n");
    MsgService.sendMsg("umf:" + String(this->U));
    off();
    irrigazione==false;
    pompaopen=false;
    this->pos=0;
    this->i=0;
    this->timer=0;
    apri=false;
    chiudi=false;
  }
  
}

 void ServoMotore::tick(){
    irrigation();
};
