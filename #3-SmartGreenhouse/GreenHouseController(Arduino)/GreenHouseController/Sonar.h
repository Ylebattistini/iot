#ifndef __SONAR__
#define __SONAR__

#include "Task.h"

class Sonar:public Task{

public:
  virtual void distance()=0;
  virtual void init(int period)=0;
  virtual void tick()=0;
};

#endif
