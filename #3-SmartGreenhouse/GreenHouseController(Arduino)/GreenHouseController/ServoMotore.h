#ifndef __SERVOMOTORE__
#define __SERVOMOTORE__

#include "Arduino.h"
#include "ServoM.h"
#include"Time.h"
#include "MsgService.h"
#include "ServoTimer2.h"

class ServoMotore: public ServoM{
public:
  ServoMotore(int pin);
  void init(int period);
  void irrigation();
  void tick();
  void on();
  void setPosition(int angle);
  void off();
private:
   int servoPin;
   int timer;
   float U;
   ServoTimer2 motor; 
   int pos;
   int delta;
   int i;
};

#endif
