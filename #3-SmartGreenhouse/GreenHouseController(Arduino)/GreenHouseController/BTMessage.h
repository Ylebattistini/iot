#ifndef __BTMESSAGE__
#define __BTMESSAGE__

#include "Arduino.h"
#include "SoftwareSerial.h"
#include "Task.h"
#include "Bluetooth.h"

class BTMessage: public Task {
  
  Bluetooth* bl;
  
public:
  BTMessage(int rxd,int txd);
  void init(int basePeriod);  
  virtual void messageblue(); 
  virtual void tick();
};

#endif
