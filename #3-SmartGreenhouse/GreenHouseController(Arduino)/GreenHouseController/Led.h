#ifndef __LED__
#define __LED__

class Led{

public:
  virtual void init()=0;
  virtual void switchOn()=0;
  virtual void switchOff()=0;
  virtual void fade(int brightness)=0;
};

#endif
