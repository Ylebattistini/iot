#include"Arduino.h"
#include "Bluetooth.h"
#include "SoftwareSerial.h"

Bluetooth::Bluetooth(int rxd, int txd){
    channel = new SoftwareSerial(txd, rxd);
    pinMode(txd, INPUT);
    pinMode(rxd, OUTPUT);
    channel->begin(9600);
}

bool Bluetooth::msgAvailable(){ 
    if(channel->available() > 0){
      return true;
    }
    return false;
}

char Bluetooth::getMsg(){
  return channel->read();
}

void Bluetooth::sendMsg(String msg){
    channel->println(msg);
}
