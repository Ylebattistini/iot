#ifndef __BLUETOOTH__
#define __BLUETOOTH__

#include "SoftwareSerial.h"

class Bluetooth {

    SoftwareSerial* channel;

    public:
        Bluetooth(int rxd, int txd);
        char getMsg();
        bool msgAvailable();
        void sendMsg(String msg);
};

#endif
