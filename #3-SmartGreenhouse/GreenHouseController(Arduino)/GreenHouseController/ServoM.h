#ifndef __SERVOM__
#define __SERVOM__

#include "Task.h"

class ServoM: public Task{
public:
  virtual void init(int period)=0;
  virtual void irrigation()=0;
  virtual void tick()=0;
  virtual void on() = 0;
  virtual void setPosition(int angle) = 0;
  virtual void off() = 0;
};

#endif
