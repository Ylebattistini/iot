#ifndef __LEDIMPL__
#define __LEDIMPL__

#include "Led.h"

class LedImpl: public Led{
public:
  LedImpl(int pin);
  void init();
  void switchOn();
  void switchOff();
  void fade(int brightness);
private:
   int ledPin;
   int pin[3];
   LedImpl* led[3];
};

#endif
