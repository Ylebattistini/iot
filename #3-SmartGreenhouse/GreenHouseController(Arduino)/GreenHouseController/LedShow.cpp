#include "LedShow.h"
#include "Arduino.h"
#include "config.h"

extern bool automatic;
extern bool manual;
extern bool pompaopen;
extern int portata;
extern bool apri;
extern bool irrigazione;

LedShow::LedShow(int pin0, int pin1, int pin2){
  this->pin[0] = pin0;
  this->pin[1] = pin1;
  this->pin[2] = pin2;
}

void LedShow::init(int period){
  Task::init(period);
  for (int i = 0; i < 3; i++){
    led[i] = new LedImpl(pin[i]);
  }
  stato = 0;
}

void LedShow::action(){
  if(automatic==true){
    led[stato]->switchOn();
    led[stato+1]->switchOff();
    led[stato+2]->switchOff();
    stato=0;
  }

  if (manual==true){
    led[stato]->switchOff();
    led[stato+1]->switchOff();
    led[stato+2]->switchOn();
    stato=0;
  }

  if((automatic==true && irrigazione==true) || ( manual==true && apri==true)){
    stato=0;
    led[stato+1]->fade(portata*40);
  }

  if(automatic==true && pompaopen==false){
    led[stato]->switchOn();
    led[stato+1]->switchOff();
    led[stato+2]->switchOff();
    stato=0;
  }

  if(manual==true && apri==false){
    led[stato]->switchOff();
    led[stato+1]->switchOff();
    led[stato+2]->switchOn();
    stato=0;
  }
}

void LedShow::tick(){
  action();
};
