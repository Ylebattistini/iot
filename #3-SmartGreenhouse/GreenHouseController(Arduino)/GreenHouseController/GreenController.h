#ifndef __GREENCONTROLLER__
#define __GREENCONTROLLER__

#include"Task.h"

class GreenController: public Task{
public:
  GreenController();
  void init(int period);
  void tick();
};
#endif
