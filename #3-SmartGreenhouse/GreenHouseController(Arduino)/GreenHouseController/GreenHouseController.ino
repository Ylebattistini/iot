#include "Scheduler.h"
#include "LedImpl.h"
#include "SonarImpl.h"
#include "config.h"
#include "LedShow.h"
#include "GreenController.h"
#include "ServoMotore.h"
#include "BTMessage.h"


Scheduler sched;
event stato;
bool manual;
bool automatic;
bool pompaopen;
bool apri;
bool chiudi;
int portata;
float umidita;
bool irrigazione;
bool connectedB;

void setup(){

  sched.init(25);
  Serial.begin(9600);
  while (!Serial){}
  Serial.println("ready to go."); 
  stato = event::AUTO;
  manual=false;
  automatic=true;
  pompaopen=false;
  apri=false;
  chiudi=false;
  portata=0;
  umidita=0.0;
  irrigazione=false;
  connectedB=false;
  
  Task* t1;
  Task* t2;
  Task* t3;
  Task* t4;
  Task* t5;
  Task* t6; 


  t1 = new SonarImpl(ECHO_PIN, TRIG_PIN);
  t1->init(50);
  sched.addTask(t1);
  
  t2= new LedShow(LED1_PIN, LED2_PIN, LEDm_PIN);
  t2->init(50);
  sched.addTask(t2);
 
  t3 = new ServoMotore(SERVO);
  t3->init(50);
  sched.addTask(t3);

  t4 = new BTMessage(RXD, TXD);
  t4->init(50);
  sched.addTask(t4);

  t5 = new GreenController();
  t5->init(50);
  sched.addTask(t5);

  
}

void loop(){
  sched.schedule();
}
