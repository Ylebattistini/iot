#include"LedImpl.h"
#include"Arduino.h"

LedImpl::LedImpl(int pin){
  this->ledPin=pin;
  init();
}

void LedImpl::init(){

  digitalWrite(ledPin, LOW);
  pinMode(ledPin, OUTPUT);
}

void LedImpl::switchOn(){
  digitalWrite(ledPin, HIGH);
}

void LedImpl::switchOff(){
  digitalWrite(ledPin, LOW);
}

void LedImpl::fade(int brightness){
  analogWrite(ledPin, brightness);
};
