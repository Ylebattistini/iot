#include"SonarImpl.h"
#include"Arduino.h"
#include"config.h"

extern bool manual;
extern bool automatic;
extern bool apri;
extern bool pompaopen;
extern bool connectedB;

SonarImpl::SonarImpl(int echoPin, int trigPin){
  this->echoPin=echoPin;
  this->trigPin=trigPin;
}

void SonarImpl::init(int period){
  Task::init(period);
  this->duration=0;
  this->distances=0;
  this->dist=0;
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
}

void SonarImpl::distance(){
  /* invio impulso */
  digitalWrite(trigPin,LOW);
  delayMicroseconds(3);
  digitalWrite(trigPin,HIGH);
  delayMicroseconds(5);
  digitalWrite(trigPin,LOW);

  /* ricevi l’eco */
  duration = pulseIn(echoPin, HIGH);
  dist = ((duration / 1000.0 / 1000.0 / 2)*vs);
  
  distances=(average*0.7)+(dist*0.3);
  average=distances;
  
  if(distances<=DIST && connectedB==true){
    manual=true;
    automatic=false;
    pompaopen=false;
    return;
  }else{
    automatic=true;
    manual=false;
    apri=false;
    return;
  }
}


void SonarImpl::tick(){
    distance();
};
