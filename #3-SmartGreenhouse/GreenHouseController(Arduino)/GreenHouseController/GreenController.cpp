#include"Arduino.h"
#include"GreenController.h"
#include "MsgService.h"
#include "config.h"

extern event stato;
extern bool manual;
extern bool automatic;

GreenController::GreenController(){
    automatic=true;
    manual=false;
}

void GreenController::init(int period){
  Task::init(period);
}

void GreenController::tick(){
  switch(stato){
    case event::AUTO:
                if(manual==true){
                  automatic=false;
                  stato=event::MANUAL;
                  break;
                }
                break;
    case event::MANUAL:
                  if(automatic==true){ 
                    stato=event::AUTO;
                    manual=false;
                    break;
                  }
                  break;
  
   }
};
