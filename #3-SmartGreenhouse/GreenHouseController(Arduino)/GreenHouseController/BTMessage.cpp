#include"Arduino.h"
#include"config.h"
#include "BTMessage.h"
#include "SoftwareSerial.h"
#include "Bluetooth.h"

extern bool pompaopen;
extern bool apri;
extern bool chiudi;
extern int portata;
extern bool manual;
extern float umidita;
extern bool connectedB;
 

BTMessage::BTMessage(int rxd, int txd){
  bl = new Bluetooth(rxd, txd);
}

void BTMessage::init(int period){
    Task::init(period);
}

void BTMessage::messageblue(){
  if (bl->msgAvailable()) {
    char msg = bl->getMsg();
    Serial.println(msg);    
    switch(msg){
      /*legenda:
       * y=>automatico
       * z=>manuale
       * m=>min
       * n=>med
       * o=>max
       * a=>aperto
       * c=>chiuso
       */
      case 'y':
        {
          connectedB=false;
          break;
        }
      case 'z':
        {
          connectedB=true;
          break;
        }
      case 'm':
        {
          if(manual==true){
           portata=Pmin;
          }
          break;
        }
      case 'n':
        {
          if(manual==true){
            portata=Pmed;
          }
          break;
        }
      case 'o':
        {
          if(manual==true){
           portata=Pmax;
         }
          break;
        }
      case 'a':
        { 
          if(manual==true){
           apri=true;
            pompaopen=false;
          }
          break;
        }
      case 'c':
        {
          if(manual==true){
            apri=false;
            chiudi=true;
            pompaopen=false;
          }
          break;
        }      
    }
    bl->sendMsg(String(umidita));
  }
   
}

void BTMessage::tick(){
   messageblue();
};
