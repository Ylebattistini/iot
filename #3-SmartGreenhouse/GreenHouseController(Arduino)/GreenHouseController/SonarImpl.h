#ifndef __SONARIMPL__
#define __SONARIMPL__

#include "Sonar.h"
#include "Task.h"
#include "MsgService.h"


class SonarImpl: public Sonar{
public:
  SonarImpl(int echoPin, int trigPin);
  void distance();
  void init(int period);
  void tick();
private:
   const float vs=331.5+0.6*20;
   int echoPin;
   int trigPin;
   unsigned long duration;
   float distances;
   int i;
   float dist;
   int average;
};

#endif
