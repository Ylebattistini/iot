#ifndef __LEDSHOW__
#define __LEDSHOW__

#include "Task.h"
#include "LedImpl.h"

class LedShow: public Task {

  int pin[3];
  LedImpl* led[3];
  int stato;

public:

  LedShow(int pin0, int pin1, int pin2);
  void init(int period);
  void tick();
  void action();
};

#endif
