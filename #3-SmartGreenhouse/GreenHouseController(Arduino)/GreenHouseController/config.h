#ifndef __CONFIG__
#define __CONFIG__

#define LED1_PIN 13
#define LED2_PIN 9
#define LEDm_PIN 11 //rosso
#define ECHO_PIN 5
#define TRIG_PIN 6
#define SERVO 3
#define Pmin 60
#define Pmed 120
#define Pmax 180
#define RXD 4
#define TXD 2

#define MAX_TASK 8
#define DIST 0.3
#define Tmax 5
#define Umin 30
#define DeltaU 5

enum class event {AUTO, MANUAL};
#endif
