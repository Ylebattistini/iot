package it.unibo.seeiot.mobileapp;

import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.content.Context;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.view.View;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.os.AsyncTask;
import android.support.annotation.Nullable;


import java.util.UUID;

import it.unibo.seeiot.mobileapp.utils.*;
import it.unibo.seeiot.mobileapp.btlib.*;
import it.unibo.seeiot.mobileapp.exceptions.*;

/*
tramite BluetoothAdapter:
 1)verificare se la comunicazione è disponibile per lo specifico device
 2)richiedere l'abilitazione all'utente se bluetooth disabilitato
 */

public class MainActivity extends AppCompatActivity {

    protected BluetoothChannel btChannel;
    private String stato = "non connesso ";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();

        //initialize BT
        if(btAdapter != null && !btAdapter.isEnabled()) {
            //bluetooth not supported
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), C.bluetooth.ENABLE_BT_REQUEST);
        }

        try {
            connectToBTServer();
        } catch (BluetoothDeviceNotFound bluetoothDeviceNotFound) {
            bluetoothDeviceNotFound.printStackTrace();
        }

        initUI();
    }

   //inizializzazione elementi nell'UI, collegamenti a funzioni di esecuzione dei comandi
    private void initUI() {
        findViewById(R.id.man).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chiamata al canale per inviare il messaggio
                stato = "manuale ";
                toMan();
                btChannel.sendMessage("z");
            }
        });

        findViewById(R.id.auto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //chiamata al canale per inviare il messaggio
                stato = "automatico ";
                toAuto();
                btChannel.sendMessage("y");
            }
        });

        Switch pompa = findViewById(R.id.pompa);
        pompa.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    String portata = findViewById(R.id.portata).toString();
                    portata="min";
                    if(portata == "min") {
                        portata = "m";
                    }
                    if(portata == "med") {
                        portata = "n";
                    }
                    if(portata == "max") {
                        portata = "o";
                    }

                    btChannel.sendMessage(portata);
                    btChannel.sendMessage("a");
                } else {
                    btChannel.sendMessage("c");
                }
            }
        });

        toAuto();
        ((TextView) findViewById(R.id.status)).setText(String.format("Status : " + stato));
    }

    //abilita e disabilita i tasti necessari
    private void toMan(){
        ((TextView) findViewById(R.id.status)).setText(String.format("Status : " + stato));
        findViewById(R.id.pompa).setEnabled(true);
        findViewById(R.id.portata).setEnabled(true);
        findViewById(R.id.auto).setEnabled(true);
        findViewById(R.id.man).setEnabled(false);
    }

    private void toAuto(){
        ((TextView) findViewById(R.id.status)).setText(String.format("Status : " + stato));
        findViewById(R.id.pompa).setEnabled(false);
        findViewById(R.id.portata).setEnabled(false);
        findViewById(R.id.auto).setEnabled(false);
        findViewById(R.id.man).setEnabled(true);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_OK){
            Log.d(C.APP_LOG_TAG, "Bluetooth enabled!");
        }

        if(requestCode == C.bluetooth.ENABLE_BT_REQUEST && resultCode == RESULT_CANCELED){
            Log.d(C.APP_LOG_TAG, "Bluetooth not enabled!");
        }
    }

    private void connectToBTServer() throws BluetoothDeviceNotFound {
        final BluetoothDevice serverDevice = BluetoothUtils.getPairedDeviceByName(C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME);
        final UUID uuid = BluetoothUtils.generateUuidFromString(C.bluetooth.BT_SERVER_UUID);

        AsyncTask<Void, Void, Integer> execute = new ConnectToBluetoothServerTask(serverDevice, uuid, new ConnectionTask.EventListener() {
            @Override
            public void onConnectionActive(final BluetoothChannel channel) {
                stato = "connesso ";
                ((TextView) findViewById(R.id.status)).setText(String.format("Status : " + stato));

                btChannel = channel;
                btChannel.registerListener(new RealBluetoothChannel.Listener() {
                    @Override
                    public void onMessageReceived(String receivedMessage) {
                        ((TextView) findViewById(R.id.umidita)).setText(String.format(receivedMessage));
                    }

                    @Override
                    public void onMessageSent(String sentMessage) { }
                });
            }

            @Override
            public void onConnectionCanceled() {
                ((TextView) findViewById(R.id.status)).setText(String.format("Status : unable to connect, device %s not found!",
                        C.bluetooth.BT_DEVICE_ACTING_AS_SERVER_NAME));
            }
        }).execute();
    }

    @Override
    public void onStart(){
        Log.i("GreenHouseMobileApp", "onStart");
        super.onStart();
    }

    @Override
    public void onResume(){
        Log.i("GreenHouseMobileApp", "onResume");
        super.onResume();
    }

    @Override
    public void onStop(){
        super.onStop();
        btChannel.close();
    }
}
